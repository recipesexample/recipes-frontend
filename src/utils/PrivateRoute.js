import React, { useGlobal, getGlobal } from "reactn";
import PropTypes from "prop-types";
// import { Route } from "react-router-dom";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect
  } from "react-router-dom";

const PrivateRoute = ({ component: Component, path, ...rest }) => {

    const isAuthenticated = getGlobal().userIsLoggedIn;
    const render = props =>
        isAuthenticated === true ? <Component {...props} /> : <Redirect to={'/login'} />;

    return <Route path={path} render={render} {...rest} />;
};

PrivateRoute.propTypes = {
    component: PropTypes.oneOfType([PropTypes.element, PropTypes.func])
        .isRequired,
    path: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.arrayOf(PropTypes.string)
    ]).isRequired
};

export default PrivateRoute;
