module.exports = {
  siteRoot: 'https://app.recipes.com',
  siteMeta: {
    title: 'Recipes',
    author: 'Christopher Hurrell ',
    image: '/image.png',
    description: 'Browse a selection of recipes',
    siteUrl: 'https://app.recipes.com',
    social: {
      twitter: 'recipeslist'
    },
    postsPerPage: 5
  },
  api: {
    dev: "http://localhost:3030",
    prod: "https://api.recipes.com"
  },
  forms: {
    insert: {
      fields: [
        {
          title: "title",
          type: "text",
          label: "Title",
          description: "e,g Fig & prosciutto pizzettas",
          value: ""
        },
        {
          title: "description",
          type: "text",
          label: "Description",
          description: "e.g Impress when entertaining with these mini pizzas topped with creamy mozzarella, sweet fruit and salty Italian ham.",
          value: ""
        },
        {
          title: "image",
          type: "text",
          label: "Image",
          description: "e.g https://xxxx.com/img/recipe.png",
          value: ""
        },
        {
          title: "cuisine",
          type: "text",
          label: "Cuisine",
          description: "e.g italian",
          value: ""
        },
        {
          title: "ingredientsItems",
          type: "array",
          label: "Ingredients",
          description: "e.g garlic",
          value: [""]

        },
        {
          title: "ingredients",
          type: "array",
          label: "Prepared Ingredients",
          description: "e.g 2 garlic cloves, crushed",
          value: [""]

        },
        {
          title: "method",
          type: "array",
          label: "Method Steps",
          description: "",
          value: [""]
        }
      ]
    }
  }
}
